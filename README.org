#+STARTUP: overview indent
#+TAGS: noexport(n) deprecated(d)
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport

* Introduction

This is the companion code/data repository for the paper entitled
*Optimization of a Radiofrequency Ablation FEM Application Using
Parallel Sparse Solvers* by Marcelo Cogo Miletto, Lucas Mello Schnorr,
and Claudio Schepke. The manuscript has been submitted in 2nd August
2020 for publication in the [[http://hpcs2020.cisedu.info/][HPCS 2020]] and accepted in
February 2021. The source of the paper is [[./ieee.org][available here]]. You'll need
Emacs+ORG to generate the TEX file for compilation.

The code snippets presented in this repository are written in R, and
can be used to generate the Figures from the paper. All figures
require no external manipulation and scripts below should be able to
generated them faithfully (assuming a recent installation of R and
required packages).

* Requirements

This companion material is written in the R programming language, and
all the scripts make heavy use of the [[https://www.tidyverse.org/][tidyverse]]. In order to reproducae
the work contained in this repository, both environments should be
installed. We also use the [[https://cran.r-project.org/web/packages/patchwork/index.html][patchwork]] and [[https://cran.r-project.org/web/packages/magick/index.html][magick]] for figure
creation. 

This work was tested in the following R environment:

#+BEGIN_SRC R :results output :exports both
library(tidyverse)
library(ggplot2)
library(patchwork)
library(magick)
library(akima)
library(wesanderson)
library(scales)
library(RColorBrewer)
library(gghighlight)
library(gstat)
library(sp)

sessionInfo()
#+END_SRC

#+RESULTS:
#+begin_example
R version 3.6.3 (2020-02-29)
Platform: x86_64-pc-linux-gnu (64-bit)
Running under: Ubuntu 20.04.1 LTS

Matrix products: default
BLAS:   /usr/lib/x86_64-linux-gnu/blas/libblas.so.3.9.0
LAPACK: /usr/lib/x86_64-linux-gnu/lapack/liblapack.so.3.9.0

locale:
 [1] LC_CTYPE=pt_BR.UTF-8       LC_NUMERIC=C              
 [3] LC_TIME=pt_BR.UTF-8        LC_COLLATE=en_US.UTF-8    
 [5] LC_MONETARY=pt_BR.UTF-8    LC_MESSAGES=en_US.UTF-8   
 [7] LC_PAPER=pt_BR.UTF-8       LC_NAME=C                 
 [9] LC_ADDRESS=C               LC_TELEPHONE=C            
[11] LC_MEASUREMENT=pt_BR.UTF-8 LC_IDENTIFICATION=C       

attached base packages:
[1] stats     graphics  grDevices utils     datasets  methods   base     

other attached packages:
 [1] sp_1.4-2           gstat_2.0-6        gghighlight_0.3.0  RColorBrewer_1.1-2
 [5] scales_1.1.1       wesanderson_0.3.6  akima_0.6-2.1      magick_2.4.0      
 [9] patchwork_1.0.1    forcats_0.5.0      stringr_1.4.0      dplyr_1.0.2       
[13] purrr_0.3.4        readr_1.3.1        tidyr_1.1.2        tibble_3.0.3      
[17] ggplot2_3.3.2      tidyverse_1.3.0   

loaded via a namespace (and not attached):
 [1] zoo_1.8-8        tidyselect_1.1.0 haven_2.3.1      lattice_0.20-40 
 [5] colorspace_1.4-1 vctrs_0.3.4      generics_0.0.2   spacetime_1.2-3 
 [9] blob_1.2.1       rlang_0.4.7      pillar_1.4.6     glue_1.4.2      
[13] withr_2.2.0      DBI_1.1.0        dbplyr_1.4.4     modelr_0.1.8    
[17] readxl_1.3.1     lifecycle_0.2.0  munsell_0.5.0    gtable_0.3.0    
[21] cellranger_1.1.0 rvest_0.3.6      fansi_0.4.1      xts_0.12-0      
[25] broom_0.7.0      Rcpp_1.0.5       backports_1.1.9  jsonlite_1.7.0  
[29] FNN_1.1.3        fs_1.5.0         hms_0.5.3        stringi_1.4.6   
[33] grid_3.6.3       cli_2.0.2        tools_3.6.3      magrittr_1.5    
[37] crayon_1.3.4     pkgconfig_2.0.3  ellipsis_0.3.1   xml2_1.3.2      
[41] reprex_0.3.0     lubridate_1.7.9  assertthat_0.2.1 httr_1.4.2      
[45] rstudioapi_0.11  R6_2.4.1         intervals_0.15.2 compiler_3.6.3  
#+end_example

* Clone this repository

The [[./data/]] directory on this repository contains all data to generate
the figures, whose total size is about 426 MBytes. 

To clone:
#+begin_src shell :results output :exports both
git clone https://gitlab.com/mmiletto/hpcs2020.git
#+end_src

* Dataset Description

These are the collected data to generate the figures
presented in the paper, described by table that follows:

|--------+---------+---------------+--------------------------------------------------------------------------------------|
| Figure | Section | Directory     | Description                                                                          |
|--------+---------+---------------+--------------------------------------------------------------------------------------|
|      3 | III.B   | [[./data/fig3/]]  | Best value for m in GMRES(m) for mesh A and B                                        |
|      4 | IV      | [[./data/fig4/]]  | PSNR value for an execution of mesh A                                                |
|      5 | IV      | [[./data/fig5/]]  | PSNR value for an execution of mesh B                                                |
|      6 | IV      | [[./data/fig6/]]  | Original values calculated for temperature and voltage at step 177 with Y fixed in 0 |
|      7 | IV      | [[./data/fig7/]]  | Numerical differences for mesh A step 177                                            |
|      8 | IV      | [[./data/fig8/]]  | Numerical differences for mesh A step 177 for ~MAGMA_low_tol~                          |
|      9 | V       | [[./data/fig9/]]  | Average makespan for the different machines and solvers                              |
|     10 | V       | [[./data/fig10/]] | Speedup for the different machines and solvers                                       |
|     11 | V       | [[./data/fig11]]/ | Traced regions duration in different machines and solvers                            |
|--------+---------+---------------+--------------------------------------------------------------------------------------|

* Paper Figures

| Figure | Script    |
|--------+-----------|
|      3 | [[./fig3.R]]  |
|      4 | [[./fig4.R]]  |
|      5 | [[./fig5.R]]  |
|      6 | [[./fig6.R]]  |
|      7 | [[./fig7.R]]  |
|      8 | [[./fig8.R]]  |
|      9 | [[./fig9.R]]  |
|     10 | [[./fig10.R]] |
|     11 | [[./fig11.R]] |

Regenerate all figures:

#+begin_src shell :results output
for script in fig3.R fig4.R fig5.R fig6.R fig7.R fig8.R fig9.R fig10.R fig11.R; do
  Rscript $script
done
#+end_src

#+RESULTS:
#+begin_example
[1] "See: imgs/best_value_stagnation.png"
[1] "data/fig4/PSNR_15_MAGMA10.csv.gz"
[1] "data/fig4/PSNR_15_MAGMA6.csv.gz"
[1] "data/fig4/PSNR_CUSOLVER_FMADOFF.csv.gz"
[1] "See: imgs/exp17_PSNR_15.png"
[1] "data/fig5/PSNR_MAIOR_MAGMA_5_1e-10.csv.gz"
[1] "data/fig5/PSNR_MAIOR_MAGMA_5_1e-6.csv.gz"
[1] "data/fig5/PSNR_MAIOR_5_CUSOLVER_fmadOFF.csv.gz"
[1] "See: imgs/exp17_PSNR_MAIOR_5.png"
[1] "See: imgs/Original_step177_fixedY.png"
[1] "See: imgs/fig7.png"
[1] "See: imgs/diff_low_tol.png"
[1] "See: imgs/exec_time_machine.png"
[1] "See: imgs/speedup.png"
 [1] "CUSOLVER_bin_draco6_15_900_2cmWithCone.csv"  
 [2] "CUSOLVER_bin_hype4_5_900_2cmWithCone.csv"    
 [3] "CUSOLVER_bin_tupi1_5_900_2cmWithCone.csv"    
 [4] "MAGMA_bin_draco7_15_900_2cmWithCone1e-10.csv"
 [5] "MAGMA_bin_draco7_15_900_2cmWithCone1e-6.csv" 
 [6] "MAGMA_bin_hype5_15_900_2cmWithCone1e-6.csv"  
 [7] "MAGMA_bin_hype5_5_900_2cmWithCone1e-10.csv"  
 [8] "MAGMA_bin_tupi1_15_900_2cmWithCone1e-6.csv"  
 [9] "MAGMA_bin_tupi1_5_900_2cmWithCone1e-10.csv"  
[10] "QRM_bin_draco7_15_900_2cmWithCone.csv"       
[11] "QRM_bin_hype4_5_900_2cmWithCone.csv"         
[12] "QRM_bin_tupi1_5_900_2cmWithCone.csv"         
 [1] "CUSOLVER_bin_draco4_15_900_2cmWithConeMenor.csv"  
 [2] "CUSOLVER_bin_hype5_15_900_2cmWithConeMenor.csv"   
 [3] "CUSOLVER_bin_tupi1_15_900_2cmWithConeMenor.csv"   
 [4] "MAGMA_bin_draco4_15_900_2cmWithConeMenor1e-10.csv"
 [5] "MAGMA_bin_draco7_15_900_2cmWithConeMenor1e-6.csv" 
 [6] "MAGMA_bin_hype5_15_900_2cmWithConeMenor1e-10.csv" 
 [7] "MAGMA_bin_hype5_15_900_2cmWithConeMenor1e-6.csv"  
 [8] "MAGMA_bin_hype5_5_900_2cmWithConeMenor_1e-10.csv" 
 [9] "MAGMA_bin_hype5_5_900_2cmWithConeMenor_1e-6.csv"  
[10] "MAGMA_bin_tupi1_15_900_2cmWithConeMenor1e-10.csv" 
[11] "MAGMA_bin_tupi1_15_900_2cmWithConeMenor1e-6.csv"  
[12] "QRM_bin_draco4_15_900_2cmWithConeMenor.csv"       
[13] "QRM_bin_hype5_15_900_2cmWithConeMenor.csv"        
[14] "QRM_bin_tupi1_15_900_2cmWithConeMenor.csv"        
[1] "See: imgs/trace_results.png"
#+end_example

* Extras

There are some tables and figures are not part of the paper. However,
they represent some facts that we mentioned along the paper. The
detailed analysis of them is located in [[./extras/Extra.org]]. 

** Figures mentioned in the paper

Here the parts mentioned in the text and their respective figures:

- The reduction of around 20% of execution time for the ~MAGMA_low_tol~
  in Section V refers to:
  #+attr_html: :width 300px
  [[./extras/solve_region_duration_distrib.png]]

- The significant amount of time that memory copies take, nearly as
  much the assembly time can be observed in
  #+attr_html: :width 300px
  [[./extras/memcpys.png]]

** PSNR computation
The binary file reader, converter, and PSNR computation are done
with the file [[./utils/bin_PSNR.cpp]]. We compile it using

#+begin_src shell :results output :exports both
cd utils
g++ -o bin_PSNR bin_PSNR.cpp -lm
ls
#+end_src

#+RESULTS:
: bin_PSNR
: bin_PSNR.cpp

Run it to generate the PSNR.csv results, the numerical diff
results, and the numerical simulated values as a csv file:
#+begin_src shell :results output :exports both
cd data/fig4
cp Results_MAGMA10.bin Results_MAGMA.bin
../../utils/bin_PSNR > PSNR_meshA_MAGMA_1e-10.csv
ls -larth
#+end_src

#+RESULTS:
#+begin_example
-rw-rw-r--  1 marcelo marcelo 103M set  1 20:12 Results_scientific.csv
-rw-rw-r--  1 marcelo marcelo  64M set  1 20:12 Results_diff.csv
-rw-rw-r--  1 marcelo marcelo  28K set  1 20:12 PSNR_meshA_MAGMA_1e-10.csv
#+end_example

