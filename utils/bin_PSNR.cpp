#include <iostream>
#include <stdio.h>
#include <stdlib.h> 
#include <fstream>
#include <math.h>
#include <cstring>
#include <iomanip>

using namespace std;

void get_Nnodes_Nsteps(int* Nnodes, int* Nsteps) {
  // try to open the file
  fstream bin_file;
  bin_file.open("Results_Original.bin", ios::in | ios::binary);
  if (!bin_file.is_open()){ 
    fprintf(stderr, "cannot open results_bin_file for writing\n");  
    exit(0);
  }
  
  // get the values from the file
  bin_file.seekg(-sizeof(int), ios::end);
  bin_file.read((char *) Nsteps, sizeof(int));
  bin_file.seekg(ios::beg);  
  bin_file.read((char *) Nnodes, sizeof(int));
  
  bin_file.close();
  return;
}

void read_results_bin(char* filename, double* volt, double* temp) {
  int Nnodes, Nsteps;

  // try to open the file
  fstream bin_file;
  bin_file.open(filename, ios::in | ios::binary);
  if (!bin_file.is_open()){ 
    fprintf(stderr, "cannot open results_bin_file for writing\n");  
    exit(0);
  }

  // read nsteps and nnodes
  bin_file.seekg(-sizeof(int), ios::end);
  bin_file.read((char *) &Nsteps, sizeof(int));
  bin_file.seekg(ios::beg);  
  bin_file.read((char *) &Nnodes, sizeof(int));

  for(int step=0; step<Nsteps; step++) {
    for(int node=0; node<Nnodes; node++) {
      bin_file.read((char *) &volt[step * Nnodes + node], sizeof(double));
      bin_file.read((char *) &temp[step * Nnodes + node], sizeof(double));
    }
  }
  bin_file.close();
}

double* PSNR(int Nnodes, int Nsteps, double* A, double* B) 
{
  // PSNR = 10 * log10 (MAX(A)^2 / MSE) 
  // PSNR = 20 * log10 (MAX(A) / SQRT(MSE))
  double* MSE = new double[Nsteps];
  double* PSNR = new double[Nsteps];
  double* MAX = new double[Nsteps];

  // MSE will be calculated for each time step
  for(int step=0; step<Nsteps; step++) {
    MSE[step] = 0.0;
    for (int node = 0; node < Nnodes; node++) {
      MSE[step] += pow(abs(A[step*Nnodes + node] - B[step*Nnodes + node]), 2);
      
      // To calculate PSNR we need to define MAX
      if(A[step * Nnodes + node] > MAX[step])
        MAX[step] = A[step * Nnodes + node];
    }
    MSE[step] = MSE[step]/Nnodes;
  }

  for(int step=0; step<Nsteps; step++) {
    PSNR[step] = 20 * log10(MAX[step] / sqrt(MSE[step]));
  } 

  return PSNR;
}

double* PSNR_MAX_GLOBAL(int Nnodes, int Nsteps, double* A, double* B) 
{
  // PSNR = 10 * log10 (MAX(A)^2 / MSE) 
  // PSNR = 20 * log10 (MAX(A) / SQRT(MSE))
  double* MSE = new double[Nsteps];
  double* PSNR = new double[Nsteps];
  double MAX_GLOBAL = 0.0;

  // find global max
  for(int step=0; step<Nsteps; step++) {
    for (int node = 0; node < Nnodes; node++) {
      if(A[step * Nnodes + node] > MAX_GLOBAL)
        MAX_GLOBAL = A[step * Nnodes + node];
    }
  }

  // MSE will be calculated for each time step
  for(int step=0; step<Nsteps; step++) {
    MSE[step] = 0.0;
    for (int node = 0; node < Nnodes; node++) {
      MSE[step] += pow(abs(A[step*Nnodes + node] - B[step*Nnodes + node]), 2);
    }
    MSE[step] = MSE[step]/Nnodes;
  }

  for(int step=0; step<Nsteps; step++) {
    PSNR[step] = 20 * log10(MAX_GLOBAL / sqrt(MSE[step]));
  } 

  return PSNR;
}

void add_noise(int Nnodes, int Nsteps, double* volt, double* temp, double noise) {
  for(int step=0; step<Nsteps; step++) {
    for (int node = 0; node < Nnodes; node++) {
      volt[step*Nnodes + node] += noise;
      temp[step*Nnodes + node] += noise;
    }
  }
}

void add_noise_one_high_diff(int Nnodes, int Nsteps, double* volt, double* temp, double noise) {
  int node = 1650;
  noise = Nnodes*noise;
  for(int step=0; step<Nsteps; step++) {
    volt[step*Nnodes + node] += noise;
    temp[step*Nnodes + node] += noise;
  }
}

void calculate_diffs(int Nnodes, int Nsteps, 
    double* v_original, double* v2, double* v3, double* v4,
    double* t_original, double* t2, double* t3, double* t4) {

  // try to open the file
  fstream diff_file, results_file;
  double d1,d2,d3,d4,d5,d6;
  // double results[Nnodes*Nsteps][6];

  results_file.open("Results_scientific.csv", ios::out);
  if (!results_file.is_open()){ 
    fprintf(stderr, "cannot open diff_file for writing\n");  
    exit(0);
  }

  diff_file.open("Results_diff.csv", ios::out);
  if (!diff_file.is_open()){ 
    fprintf(stderr, "cannot open diff_file for writing\n");  
    exit(0);
  }

  diff_file << "step,V_MAGMA,V_cuSOLVER,V_QRM,T_MAGMA,T_cuSOLVER,T_QRM\n";
  results_file << "step,nodeid,V_original,V_MAGMA,V_cuSOLVER,V_QRM,T_original,T_MAGMA,T_cuSOLVER,T_QRM\n";
  results_file << setprecision(15);
  diff_file << std::scientific;


  for(int step=0; step<Nsteps; step++) {
    for (int node = 0; node < Nnodes; node++) {

      d1 = v_original[step*Nnodes + node] - v2[step*Nnodes + node];
      d2 = v_original[step*Nnodes + node] - v3[step*Nnodes + node];
      d3 = v_original[step*Nnodes + node] - v4[step*Nnodes + node];
      
      d4 = t_original[step*Nnodes + node] - t2[step*Nnodes + node];
      d5 = t_original[step*Nnodes + node] - t3[step*Nnodes + node];
      d6 = t_original[step*Nnodes + node] - t4[step*Nnodes + node];
      // cout << "v_original[step*Nnodes + node] - v2[step*Nnodes + node] = " << v_original[step*Nnodes + node] << " - " << v2[step*Nnodes + node] << endl; 
      diff_file << step << "," << node+1 << "," << d1 << "," << d2 << "," << d3 << "," << d4 << "," << d5 << "," << d6 << endl;    
      results_file << step << "," << node+1 << "," << v_original[step*Nnodes + node] << "," << v2[step*Nnodes + node] << "," << v3[step*Nnodes + node] << "," << v4[step*Nnodes + node] << "," << t_original[step*Nnodes + node] << "," << t2[step*Nnodes + node] << "," << t3[step*Nnodes + node] << "," << t4[step*Nnodes + node] << endl;
    }
  } 
  diff_file.close();
  results_file.close();
}

int main(int argc, char** argv) 
{

  int Nnodes, Nsteps;
  get_Nnodes_Nsteps(&Nnodes, &Nsteps);
  int size = Nsteps * Nnodes;
  fprintf(stderr, "Mesh has %d nodes and simulation %d steps, size = %d\n", Nnodes,  Nsteps, size);

  double  *volt_Original = new double[size],
          *volt_MAGMA    = new double[size+10*Nnodes],
          *volt_CUSOLVER = new double[size],
          *volt_QRM      = new double[size],
          *volt_upper    = new double[size],
          *volt_mid      = new double[size],
          *volt_lower    = new double[size];

  double  *temp_Original = new double[size],
          *temp_MAGMA    = new double[size+10*Nnodes],
          *temp_CUSOLVER = new double[size],
          *temp_QRM      = new double[size],
          *temp_upper    = new double[size],
          *temp_mid      = new double[size],
          *temp_lower    = new double[size];

  double *PSNR_Temp_MAGMA = new double[Nsteps], 
         *PSNR_Temp_CUSOLVER = new double[Nsteps], 
         *PSNR_Temp_QRM = new double[Nsteps];

  double *PSNR_Volt_MAGMA = new double[Nsteps], 
         *PSNR_Volt_CUSOLVER = new double[Nsteps], 
         *PSNR_Volt_QRM = new double[Nsteps];
  
  double *PSNR_Temp_upper = new double[Nsteps], 
         *PSNR_Temp_mid = new double[Nsteps], 
         *PSNR_Temp_lower = new double[Nsteps];

  double *PSNR_Volt_upper = new double[Nsteps], 
         *PSNR_Volt_mid = new double[Nsteps], 
         *PSNR_Volt_lower = new double[Nsteps];

  read_results_bin((char*)"Results_Original.bin", &volt_Original[0], &temp_Original[0]); 
  read_results_bin((char*)"Results_MAGMA.bin", &volt_MAGMA[0], &temp_MAGMA[0]); 
  read_results_bin((char*)"Results_CUSOLVER.bin", &volt_CUSOLVER[0], &temp_CUSOLVER[0]); 
  read_results_bin((char*)"Results_QRM.bin", &volt_QRM[0], &temp_QRM[0]); 
  
  std::memcpy(volt_upper, volt_Original, size*sizeof(double));
  std::memcpy(volt_mid,   volt_Original, size*sizeof(double));
  std::memcpy(volt_lower, volt_Original, size*sizeof(double));
  std::memcpy(temp_upper, temp_Original, size*sizeof(double));
  std::memcpy(temp_mid,   temp_Original, size*sizeof(double));
  std::memcpy(temp_lower, temp_Original, size*sizeof(double));

  add_noise(Nnodes, Nsteps, volt_upper, temp_upper, 1e-12);
  add_noise(Nnodes, Nsteps, volt_mid,   temp_mid,   1e-8);
  add_noise_one_high_diff(Nnodes, Nsteps, volt_lower, temp_lower, 1e-5);
  // add_noise(Nnodes, Nsteps, volt_lower, temp_lower, 1e-5);
  
  PSNR_Temp_MAGMA = PSNR_MAX_GLOBAL(Nnodes, Nsteps, temp_Original, temp_MAGMA);
  PSNR_Temp_CUSOLVER = PSNR_MAX_GLOBAL(Nnodes, Nsteps, temp_Original, temp_CUSOLVER);
  PSNR_Temp_QRM = PSNR_MAX_GLOBAL(Nnodes, Nsteps, temp_Original, temp_QRM);

  PSNR_Volt_MAGMA = PSNR_MAX_GLOBAL(Nnodes, Nsteps, volt_Original, volt_MAGMA);
  PSNR_Volt_CUSOLVER = PSNR_MAX_GLOBAL(Nnodes, Nsteps, volt_Original, volt_CUSOLVER);
  PSNR_Volt_QRM = PSNR_MAX_GLOBAL(Nnodes, Nsteps, volt_Original, volt_QRM);

  PSNR_Temp_upper = PSNR_MAX_GLOBAL(Nnodes, Nsteps, temp_Original, temp_upper);
  PSNR_Temp_mid = PSNR_MAX_GLOBAL(Nnodes, Nsteps, temp_Original, temp_mid);
  PSNR_Temp_lower = PSNR_MAX_GLOBAL(Nnodes, Nsteps, temp_Original, temp_lower);

  PSNR_Volt_upper = PSNR_MAX_GLOBAL(Nnodes, Nsteps, volt_Original, volt_upper);
  PSNR_Volt_mid = PSNR_MAX_GLOBAL(Nnodes, Nsteps, volt_Original, volt_mid);
  PSNR_Volt_lower = PSNR_MAX_GLOBAL(Nnodes, Nsteps, volt_Original, volt_lower);

  // write everything in a csv format
  printf("step,PSNR_Temp_MAGMA,PSNR_Temp_CUSOLVER,PSNR_Temp_QRM,PSNR_Temp_1e-12,PSNR_Temp_1e-8,PSNR_Temp_1e-5,PSNR_Volt_MAGMA,PSNR_Volt_CUSOLVER,PSNR_Volt_QRM,PSNR_Volt_1e-12,PSNR_Volt_1e-8,PSNR_Volt_1e-5\n");
  for(int i=0; i<Nsteps; i++)
    printf("%d,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf\n", i, 
      PSNR_Temp_MAGMA[i], PSNR_Temp_CUSOLVER[i], PSNR_Temp_QRM[i], 
      PSNR_Temp_upper[i], PSNR_Temp_mid[i], PSNR_Temp_lower[i],  
      PSNR_Volt_MAGMA[i], PSNR_Volt_CUSOLVER[i], PSNR_Volt_QRM[i], 
      PSNR_Volt_upper[i], PSNR_Volt_mid[i], PSNR_Volt_lower[i]);
  
  calculate_diffs(Nnodes, Nsteps, volt_Original, volt_MAGMA, volt_CUSOLVER, volt_QRM, temp_Original, temp_MAGMA, temp_CUSOLVER, temp_QRM);

  return 0;
}
